# Devops Homework

This repository contains the solution for the DevOps Homework Assignment. We will be using Azure Kubernetes Service (AKS), GitLab CI/CD, and Flux for the CD process.

## Getting started

Before you get started, you will need:

- Azure account with a active subscription
- Azure CLI
- terraform
- kubectl
- Docker
- GitLab account
- Flux

## Step 1: Provisioning AKS Infrastructure using Terraform

We will start by provisioning an AKS cluster on Azure using Terraform. You will need to provide your Azure account credentials to Terraform and run terraform apply to provision the infrastructure.

First we need to login into az:

```
az login --tenant tennant.id
az account set --subscription=subscription.id

```
After that we need to create a terraform file (main.tf) where we tell terraform what to create. In our case this is the file: [main.tf](https://gitlab.com/devops-homework/devops-homework/-/blob/main/aks-cluster-terraform/main.tf)

When the file is created and ready to be used, we would need to do the following:
```
cd aks-cluster-terraform/ (where the main.tf is located)
terraform init
terraform apply
```
The cluster should create itself. Check if you can connect to the cluster:
```
az aks get-credentials --resource-group devopsRG --name AKSdevops
```
This will set up the credentials that you need to interact with the cluster, for example kubectl should be ready to use.

## Step 2: Dockerizing the Applications

The applications are located in the apps/app_a and apps/app_b directories. Each directory contains a Dockerfile that describes how to build a Docker image for the application:
```
cd apps/app_a
docker build -t app_a:latest .
cd apps/app_b
docker build -t app_b:latest .
```

Now that we have the images we should tag them (you will need a docker account):
```
docker login
docker tag app_a:latest yourusername/app_a:latest
docker tag app_b:latest yourusername/app_b:latest
```
Let's push the images so we can use them in our kubernetes deployments:
```
docker push yourusername/app_a:latest
docker push yourusername/app_b:latest
```
Now we have the docker images pushed and we can just reffer to them in the deployments.

## Step 3: Flux install

We will use flux for the Continuous Delivery. Flux need to be bootstraped to a repository (in this case a gitlab repo):
```
flux bootstrap gitlab --owner=devops-homework --repository=devops-homework --branch=main --path=clusters/my-cluster --token-auth --personal
```
The token needs to be generated in Gitlab and set up as an env variable on your system. export GITLAB_TOKEN=<your-token>



## Step 4: Deploying the Applications

The Kubernetes deployment configurations for the applications are located in the clusters/my-cluster/prod/a-app and clusters/my-cluster/prod/b-app. Each directory contains a deployment.yaml file that describes how to deploy the respective Docker image to the Kubernetes cluster.

To apply de deployment (without flux) in the cluster we need to use the following command:
```
cd clusters/my-cluster/prod/a-app
kubectl apply -f deployment.yaml
cd clusters/my-cluster/prod/b-app
kubectl apply -f deployment.yaml
```

## Step 5: Setting Up Continuous Delivery

We will use GitLab's CI/CD pipeline for Continuous Integration and Flux for Continuous Delivery. The GitLab pipeline will build the Docker images and push them to a Docker registry whenever a commit is made to the repository. Flux will continuously monitor the Docker registry and the clusters/my-cluster/prod directory in the GitLab repository. When a new image is pushed to the registry, Flux will update the image in the Kubernetes deployments.

Example of flow:
commit & push -> pipeline start, new image is created and named base on the commit sha, the image version is changed in the deployment via sed -> flux sees the modification and sends it to the cluster -> deployment runs in the cluster with the new images
***



## NOTES:
flux was not installed in this cluster/repository -> my free aks subscription ended :(