from flask import Flask, request
import sqlite3 as sql
application = Flask(__name__)

@application.route('/auth', methods=['POST'])
def auth():
    try:
        token = request.headers['Authorization']
        print(f"Token received: {token}")  # Debug line
        con = sql.connect("database.db")
        cur = con.cursor()
        cur.execute(
            "SELECT username from users where token = (?) LIMIT 1",
            (token, ))
        username = cur.fetchone()
        if username is None:
            print("No user found with this token.")  # Debug line
            return 'fail'
        print(f"Username found: {username[0]}")  # Debug line
        con.close()
        return username[0]
    except Exception as e:
        print(f"Exception: {str(e)}")  # Debug line
        return 'fail'

if __name__ == "__main__":
    application.run(host='0.0.0.0', port=5001)