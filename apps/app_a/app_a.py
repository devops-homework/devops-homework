from flask import Flask, request
import requests
application = Flask(__name__)


@application.route('/hello')
def hello():
    return 'Hello there'


@application.route('/jobs', methods=['POST'])
def jobs():
    token = request.headers['Authorization']
    headers = {"Authorization": token}
    result = requests.post('http://app-b-service:80/auth', headers=headers).content.decode('utf-8')  # decode here
    if result.strip() == "density":  # strip here to remove any leading
        return 'Jobs:\nTitle: Devops\nDescription: Awesome\n'
    else:
        return 'fail'


if __name__ == "__main__":
    application.run(host='0.0.0.0', port=5000)
