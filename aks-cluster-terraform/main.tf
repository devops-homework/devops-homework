provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "my_resource_group" {
  name     = "devopsRG"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "my_aks_cluster" {
  name                = "AKSdevops"
  location            = azurerm_resource_group.my_resource_group.location
  resource_group_name = azurerm_resource_group.my_resource_group.name
  dns_prefix          = "AKSdns"

  default_node_pool {
    name       = "nodepool"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}